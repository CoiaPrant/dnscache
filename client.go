package dnscache

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/miekg/dns"
	utls "gitlab.com/go-extension/tls"
)

const defaultTimeout = 2 * time.Second

var defaultDialer = &net.Dialer{Timeout: defaultTimeout}

func schemePort(u *url.URL) string {
	if port := u.Port(); port != "" {
		return port
	}

	switch u.Scheme {
	case "https", "uhttps":
		return "443"
	case "tls", "utls":
		return "853"
	case "quic":
		return "8853"
	default:
		return "53"
	}
}

type ClientConfig struct {
	Upstream Upstream
	Timeout  time.Duration
}

type Client struct {
	upstream Upstream
	client   *dns.Client
}

func NewClient(config *ClientConfig) *Client {
	return &Client{
		upstream: config.Upstream,
		client:   &dns.Client{Timeout: config.Timeout},
	}
}

func ParseClient(server string) (*Client, error) {
	if !strings.Contains(server, "://") {
		server = "udp://" + server
	}

	u, err := url.Parse(server)
	if err != nil {
		return nil, err
	}

	if u.Hostname() == "" {
		return nil, fmt.Errorf("parser: invalid upstream, missing hostname")
	}

	var insecure bool
	if option := u.Query().Get("insecure"); option != "" {
		insecure, err = strconv.ParseBool(option)
		if err != nil {
			return nil, err
		}
	}

	var timeout time.Duration = defaultTimeout
	if option := u.Query().Get("timeout"); option != "" {
		timeout, err = time.ParseDuration(option)
		if err != nil {
			return nil, err
		}
	}

	if timeout <= 0 {
		return nil, fmt.Errorf("parser: invalid upstream, timeout must be greater than 0")
	}

	var upstream Upstream
	switch u.Scheme {
	case "udp":
		upstream = &UDPUpstream{
			Addr:   net.JoinHostPort(u.Hostname(), schemePort(u)),
			Dialer: &net.Dialer{Timeout: timeout},
		}
	case "tcp":
		upstream = &TCPUpstream{
			Addr:   net.JoinHostPort(u.Hostname(), schemePort(u)),
			Dialer: &net.Dialer{Timeout: timeout},
		}
	case "tls":
		upstream = &TLSUpstream{
			Addr:   net.JoinHostPort(u.Hostname(), schemePort(u)),
			Config: &tls.Config{InsecureSkipVerify: insecure},
			Dialer: &net.Dialer{Timeout: timeout},
		}
	case "utls":
		upstream = &UTLSUpstream{
			Addr:   net.JoinHostPort(u.Hostname(), schemePort(u)),
			Config: &utls.Config{InsecureSkipVerify: insecure},
			Dialer: &net.Dialer{Timeout: timeout},
		}
	default:
		return nil, fmt.Errorf("parser: invalid upstream, invalid scheme %s", u.Scheme)
	}

	return &Client{
		upstream: upstream,
		client:   &dns.Client{Timeout: timeout},
	}, nil
}

func (c *Client) Exchange(m *dns.Msg) (r *dns.Msg, rtt time.Duration, err error) {
	conn, err := c.upstream.NewConn()
	if err != nil {
		return nil, 0, err
	}

	return c.client.ExchangeWithConn(m, conn)
}

func (c *Client) ExchangeContext(ctx context.Context, m *dns.Msg) (r *dns.Msg, rtt time.Duration, err error) {
	conn, err := c.upstream.NewConn()
	if err != nil {
		return nil, 0, err
	}

	return c.client.ExchangeWithConnContext(ctx, m, conn)
}
