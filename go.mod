module gitlab.com/CoiaPrant/dnscache

go 1.18

require (
	github.com/miekg/dns v1.1.58
	github.com/puzpuzpuz/xsync/v3 v3.1.0
	gitlab.com/go-extension/rand v0.0.0-20240303103951-707937a049b5
	gitlab.com/go-extension/tls v0.0.0-20240304171319-e6745021905e
)

require (
	github.com/RyuaNerin/go-krypto v1.2.4 // indirect
	github.com/andybalholm/brotli v1.1.0 // indirect
	github.com/blang/semver/v4 v4.0.0 // indirect
	github.com/cloudflare/circl v1.3.7 // indirect
	github.com/klauspost/compress v1.17.7 // indirect
	github.com/pedroalbanese/camellia v0.0.0-20220911183557-30cc05c20118 // indirect
	github.com/pmorjan/kmod v1.1.1 // indirect
	gitlab.com/go-extension/aes-ccm v0.0.0-20230221065045-e58665ef23c7 // indirect
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/mod v0.16.0 // indirect
	golang.org/x/net v0.22.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/tools v0.19.0 // indirect
)
