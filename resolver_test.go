package dnscache

import (
	"context"
	"testing"
)

func TestParseResolver(t *testing.T) {
	_, err := ParseResolver(nil, "8.8.8.8")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}

	_, err = ParseResolver(nil, "8.8.8.8:53")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}

	_, err = ParseResolver(nil, "udp://8.8.8.8")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}

	_, err = ParseResolver(nil, "udp://8.8.8.8:53")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}
}

func TestUDPResolver(t *testing.T) {
	resolver, err := ParseResolver(nil, "8.8.8.8")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}

	ips, err := resolver.LookupIP(context.Background(), "www.google.com")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}

	t.Logf("resolved www.google.com. ips: %s", ips)
}
func TestTCPResolver(t *testing.T) {
	resolver, err := ParseResolver(nil, "tcp://8.8.8.8")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}

	ips, err := resolver.LookupIP(context.Background(), "www.google.com")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}

	t.Logf("resolved www.google.com. ips: %s", ips)
}

func TestTLSResolver(t *testing.T) {
	resolver, err := ParseResolver(nil, "tls://8.8.8.8")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}

	ips, err := resolver.LookupIP(context.Background(), "www.google.com")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}

	t.Logf("resolved www.google.com. ips: %s", ips)
}

func TestUTLSResolver(t *testing.T) {
	resolver, err := ParseResolver(nil, "utls://8.8.8.8")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}

	ips, err := resolver.LookupIP(context.Background(), "www.google.com")
	if err != nil {
		t.Fatalf("create resolver failed, error: %s", err)
		return
	}

	t.Logf("resolved www.google.com. ips: %s", ips)
}
