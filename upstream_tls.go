package dnscache

import (
	"context"
	"crypto/tls"
	"net"

	"github.com/miekg/dns"
)

type TLSUpstream struct {
	Addr   string
	Config *tls.Config
	Dialer *net.Dialer
}

func (u *TLSUpstream) dialer() *tls.Dialer {
	netDialer := func() *net.Dialer {
		if u.Dialer != nil {
			return u.Dialer
		}

		return defaultDialer
	}()

	return &tls.Dialer{NetDialer: netDialer, Config: u.Config}
}

func (u *TLSUpstream) NewConn() (*dns.Conn, error) {
	conn, err := u.dialer().Dial("tcp", u.Addr)
	if err != nil {
		return nil, err
	}

	return &dns.Conn{Conn: conn}, nil
}

func (u *TLSUpstream) NewConnContext(ctx context.Context) (*dns.Conn, error) {
	conn, err := u.dialer().DialContext(ctx, "tcp", u.Addr)
	if err != nil {
		return nil, err
	}

	return &dns.Conn{Conn: conn}, nil
}
