package dnscache

import (
	"context"
	"net"

	"github.com/miekg/dns"
)

type TCPUpstream struct {
	Addr   string
	Dialer *net.Dialer
}

func (u *TCPUpstream) dialer() *net.Dialer {
	if u.Dialer != nil {
		return u.Dialer
	}

	return defaultDialer
}

func (u *TCPUpstream) NewConn() (*dns.Conn, error) {
	conn, err := u.dialer().Dial("tcp", u.Addr)
	if err != nil {
		return nil, err
	}

	return &dns.Conn{Conn: conn}, nil
}

func (u *TCPUpstream) NewConnContext(ctx context.Context) (*dns.Conn, error) {
	conn, err := u.dialer().DialContext(ctx, "tcp", u.Addr)
	if err != nil {
		return nil, err
	}

	return &dns.Conn{Conn: conn}, nil
}
