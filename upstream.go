package dnscache

import (
	"github.com/miekg/dns"
)

type Upstream interface {
	NewConn() (conn *dns.Conn, err error)
}
