package cache

import (
	"sync"
	"time"

	"github.com/puzpuzpuz/xsync/v3"
)

const checkExpiresDuration = time.Minute

type Cache[K comparable, V any] struct {
	items *xsync.MapOf[K, *Item[V]]

	callbacks struct {
		delete []func(key K, val V)
	}

	mutex sync.RWMutex
	done  chan struct{}
}

func NewCache[K comparable, V any]() *Cache[K, V] {
	cache := &Cache[K, V]{
		items: xsync.NewMapOf[K, *Item[V]](),

		done: make(chan struct{}),
	}

	go cache.checkExpires()
	return cache
}

func (cache *Cache[K, V]) AddDeleteCallback(f func(key K, val V)) {
	cache.callbacks.delete = append(cache.callbacks.delete, f)
}

func (cache *Cache[K, V]) SetDeleteCallbacks(callbacks []func(key K, val V)) {
	cache.callbacks.delete = callbacks
}

func (cache *Cache[K, V]) Add(key K, val V, lifeSpan time.Duration) {
	cache.mutex.RLock()
	defer cache.mutex.RUnlock()

	if cache.Stopped() {
		return
	}

	cache.items.Store(key, &Item[V]{
		Data:    val,
		Expires: time.Now().Add(lifeSpan),
	})
}

func (cache *Cache[K, V]) Get(key K) (val V, ok bool) {
	var item *Item[V]
	item, ok = cache.items.Compute(key, func(val *Item[V], loaded bool) (*Item[V], bool) {
		if !loaded {
			return nil, true
		}

		delete := val.Expires.Before(time.Now())
		if !delete {
			return val, false
		}

		for _, callback := range cache.callbacks.delete {
			callback(key, val.Data)
		}

		return val, true
	})
	if !ok {
		return
	}

	val = item.Data
	return
}

func (cache *Cache[K, V]) Delete(key K) (val V, ok bool) {
	var item *Item[V]
	item, ok = cache.items.LoadAndDelete(key)
	if !ok {
		return
	}

	val = item.Data
	for _, callback := range cache.callbacks.delete {
		callback(key, val)
	}

	return
}

func (cache *Cache[K, V]) Clear() {
	cache.items.Range(func(key K, _ *Item[V]) bool {
		cache.items.Compute(key, func(val *Item[V], loaded bool) (*Item[V], bool) {
			if !loaded {
				return nil, true
			}

			for _, callback := range cache.callbacks.delete {
				callback(key, val.Data)
			}

			return val, true
		})
		return true
	})
}

func (cache *Cache[K, V]) Size() int {
	return cache.items.Size()
}

func (cache *Cache[K, V]) checkExpires() {
	ticker := time.NewTicker(checkExpiresDuration)
	defer ticker.Stop()
	defer cache.Clear()

	for {
		select {
		case <-cache.done:
			return
		case <-ticker.C:
			cache.items.Range(func(key K, _ *Item[V]) bool {
				cache.items.Compute(key, func(val *Item[V], loaded bool) (*Item[V], bool) {
					if !loaded {
						return nil, true
					}

					delete := val.Expires.Before(time.Now())
					if !delete {
						return val, false
					}

					for _, callback := range cache.callbacks.delete {
						callback(key, val.Data)
					}

					return val, true
				})
				return true
			})
		}
	}
}

func (cache *Cache[K, V]) Stopped() bool {
	select {
	case <-cache.done:
		return true
	default:
		return false
	}
}

func (cache *Cache[K, V]) Stop() {
	cache.mutex.Lock()
	defer cache.mutex.Unlock()

	if cache.Stopped() {
		return
	}

	close(cache.done)
}
