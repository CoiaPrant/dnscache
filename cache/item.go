package cache

import "time"

type Item[V any] struct {
	Data    V
	Expires time.Time
}
