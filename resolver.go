package dnscache

import (
	"context"
	"errors"
	"net"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/miekg/dns"
	"gitlab.com/CoiaPrant/dnscache/cache"
	"gitlab.com/go-extension/rand"
)

const MinTTL = 600

var (
	IPv4Loopback = net.IPv4(127, 0, 0, 1)
	IPv6Loopback = net.IPv6loopback

	ErrResolverClosed = errors.New("resolver has been closed")

	defaultResolverConfig = &ResolverConfig{
		MinTTL: MinTTL,
	}
)

type ResolverConfig struct {
	MinTTL uint32
}

type ipCache = *cache.Cache[string, []net.IP]

type Resolver struct {
	config *ResolverConfig

	cache  ipCache
	backup ipCache

	clients []*Client

	mutex  sync.RWMutex
	closed atomic.Bool
}

func NewResolver(config *ResolverConfig, clients ...*Client) (*Resolver, error) {
	if config == nil {
		config = defaultResolverConfig
	}

	if config.MinTTL < MinTTL {
		config.MinTTL = MinTTL
	}

	r := &Resolver{
		config: config,

		cache:  cache.NewCache[string, []net.IP](),
		backup: cache.NewCache[string, []net.IP](),

		clients: clients,
	}

	r.cache.AddDeleteCallback(func(host string, ips []net.IP) {
		r.backup.Add(host, ips, time.Hour)
	})

	return r, nil
}

func ParseResolver(config *ResolverConfig, upstreams ...string) (*Resolver, error) {
	var clients []*Client
	for _, upstream := range upstreams {
		client, err := ParseClient(upstream)
		if err != nil {
			return nil, err
		}

		clients = append(clients, client)
	}

	return NewResolver(config, clients...)
}

func (r *Resolver) LookupIP(ctx context.Context, host string) ([]net.IP, error) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()

	if r.closed.Load() {
		return nil, ErrResolverClosed
	}

	// Pre Check
	{
		if host == "localhost" {
			return []net.IP{IPv4Loopback, IPv6Loopback}, nil
		}

		if strings.HasPrefix(host, "[") && strings.HasSuffix(host, "]") {
			if ip := net.ParseIP(host[1 : len(host)-1]); ip != nil {
				return []net.IP{ip}, nil
			}
		}

		if ip := net.ParseIP(host); ip != nil {
			return []net.IP{ip}, nil
		}
	}

	// FQDN
	host = dns.Fqdn(host)

	// Lookup Cache
	if item, ok := r.cache.Get(host); ok {
		return item, nil
	}

	{
		var ips []net.IP
		var ttl uint32

		// Lookup Internet
		{
			answers, ok := r.Lookup(ctx, host, dns.TypeA)
			if ok {
				for _, answer := range answers {
					if ar, _ := answer.(*dns.A); ar != nil {
						ips = append(ips, ar.A)

						if ttl == 0 || ar.Header().Ttl < ttl {
							ttl = ar.Header().Ttl
						}
					}
				}
			}

			answers, ok = r.Lookup(ctx, host, dns.TypeAAAA)
			if ok {
				for _, answer := range answers {
					if ar, _ := answer.(*dns.AAAA); ar != nil {
						ips = append(ips, ar.AAAA)

						if ttl == 0 || ar.Header().Ttl < ttl {
							ttl = ar.Header().Ttl
						}
					}
				}
			}
		}

		// Check Response and Set Cache
		if len(ips) > 0 {
			if ttl < r.config.MinTTL {
				ttl = r.config.MinTTL
			}

			r.cache.Add(host, ips, time.Duration(ttl)*time.Second)
			r.backup.Delete(host)
			return ips, nil
		}
	}

	// Lookup backup cache
	if item, ok := r.backup.Get(host); ok {
		return item, nil
	}

	return net.DefaultResolver.LookupIP(ctx, "ip", host)
}

func (r *Resolver) LookupAddress(ctx context.Context, host string) string {
	ips, err := r.LookupIP(ctx, host)
	if err != nil {
		return "255.255.255.255"
	}

	ip := ips[rand.Standard.IntN(len(ips))]

	if ip.To4() == nil {
		return "[" + ip.String() + "]"
	}
	return ip.String()
}

func (r *Resolver) Lookup(ctx context.Context, z string, t uint16) ([]dns.RR, bool) {
	mq := &dns.Msg{}
	mq.SetQuestion(z, t)
	return r.LookupMsg(ctx, mq)
}

func (r *Resolver) LookupMsg(ctx context.Context, mq *dns.Msg) ([]dns.RR, bool) {
	for _, client := range r.clients {
		mr, _, err := client.ExchangeContext(ctx, mq)
		if err == nil {
			return mr.Answer, true
		}
	}

	return nil, false
}

func (r *Resolver) FlushDNS(host string) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()

	if r.closed.Load() {
		return
	}

	if host == "" {
		r.FlushAll()
		return
	}

	if net.ParseIP(host) != nil {
		return
	}

	r.cache.Delete(dns.Fqdn(host))
}

func (r *Resolver) FlushAll() {
	r.mutex.RLock()
	defer r.mutex.RUnlock()

	if r.closed.Load() {
		return
	}

	r.cache.Clear()
}

func (r *Resolver) Close() {
	if r.closed.Swap(true) {
		return
	}

	r.mutex.Lock()
	defer r.mutex.Unlock()

	r.cache.Stop()
	r.backup.Stop()
}
