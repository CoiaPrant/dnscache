package dnscache

import (
	"context"
	"net"

	"github.com/miekg/dns"
)

type UDPUpstream struct {
	Addr   string
	Dialer *net.Dialer
}

func (u *UDPUpstream) dialer() *net.Dialer {
	if u.Dialer != nil {
		return u.Dialer
	}

	return defaultDialer
}

func (u *UDPUpstream) NewConn() (*dns.Conn, error) {
	conn, err := u.dialer().Dial("udp", u.Addr)
	if err != nil {
		return nil, err
	}

	return &dns.Conn{Conn: conn}, nil
}

func (u *UDPUpstream) NewConnContext(ctx context.Context) (*dns.Conn, error) {
	conn, err := u.dialer().DialContext(ctx, "udp", u.Addr)
	if err != nil {
		return nil, err
	}

	return &dns.Conn{Conn: conn}, nil
}
