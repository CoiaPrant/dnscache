package dnscache

import (
	"context"
	"net"

	"github.com/miekg/dns"
	utls "gitlab.com/go-extension/tls"
)

type UTLSUpstream struct {
	Addr   string
	Config *utls.Config
	Dialer *net.Dialer
}

func (u *UTLSUpstream) dialer() *utls.Dialer {
	netDialer := func() *net.Dialer {
		if u.Dialer != nil {
			return u.Dialer
		}

		return defaultDialer
	}()

	return &utls.Dialer{NetDialer: netDialer, Config: u.Config}
}

func (u *UTLSUpstream) NewConn() (*dns.Conn, error) {
	conn, err := u.dialer().Dial("tcp", u.Addr)
	if err != nil {
		return nil, err
	}

	return &dns.Conn{Conn: conn}, nil
}

func (u *UTLSUpstream) NewConnContext(ctx context.Context) (*dns.Conn, error) {
	conn, err := u.dialer().DialContext(ctx, "tcp", u.Addr)
	if err != nil {
		return nil, err
	}

	return &dns.Conn{Conn: conn}, nil
}
